# Homework4

ECSE6961 Homework 4


Quickstart
---

**prerequisites**: `gcc`, `make`, and a POSIX system

The program can be built by running `make` in this repository's directory. To run the compiled program use:

For encoding:
`./dict e infile dict_out encode_out`

For decoding:
`./dict d dict_in encode_in outfile`

For interacting with an encoded dictionary:
`./dict i dict_in`
then enter queries in stdin and terminate with EOF (Ctrl-D)

Description
---

The presented program encodes a data in to a dictionary that allows for compression and efficient query. The data input file expects strings <=16 characters long separated by newlines. Running the program in encoding mode produces two files, a dictionary and the encoded data. The dictionary file is a space efficient store of the strings in the data file. The encoded file is a space efficient representation of the data file and can be decoded efficiently using the dictionary. 

To encoded the data file, a hash table is used. The size of the hash table is determined automatically from the input file size. The hash table uses the strings as keys and gives their encoded dictionary key as a value. The dictionary keys are assigned sequentially and used to index strings stored in adjacent memory. As such, decoding a dictionary key is an O(1) operation. Encoding or querying a string is O(1) in the best case and O(n) in the worse.  

Code overview
---
Two data structures are implemented and an overview of their structure is presented here. 

The first data structure is the hash table, defined in `htable.h`. To create a hash table the function `htable_t* create_htable(size_t size, hashf_t hashf)` will return a pointer to a hash table of a specified size and pointer to hash function. Note that the size of the hash table heavily effects the performance. The hash table stores keys as `__m128i`, used as strings in the program, and gives two integer values as keys. The keys and values are stored in a struct `bucket_t`. To insert a key into the hash table `bucket_t* insert_dat(htable_t* table, __m128i dat)` is used. If the key is already in the hash table, a reference to the key is returned, otherwise a new `bucket_t` is created and a reference to that returned. To find a key, `bucket_t* find(htable_t* table, __m128i dat)` is used. `NULL` is returned if no key is found.

The next data structure implemented is an array of mutable size, found in `vector.h`. To create a new vector `vector_t* new_vector()` is used. The vector stores `__m128i` elements in adjacent memory. To access data in the vector the vector's struct `dat` field is subscripted by the index. To add an element to the end of the vector, `element_t* push_back(vector_t* vector, __m128i dat)` is used. `push_back` will resize the vector if needed. 

Encoding a file will read a line from the data file and try to insert it into the hash table. If the key was not present in the hash table, the string is added to the dictionary vector with `push_back` and its encoded value becomes its index in the dictionary. If the key is already in the hash table, it can be replaced by the associated encoded value. Encoding produces and "encoded file" and "dictionary file". The format of the two files is below:

Encoded file
* 4 bytes : `ENCO`
* encoded strings - for each
  * sizeof(int) bytes : encoded value


Dictionary file
* 4 bytes : `DICT`
* sizeof(size_t) bytes : length of dictionary
* dictionary entries, in order
  * 1 byte : length of string
  * sizeof(int) : occurrences in original file
  * variable bytes : string (not null terminated)

The dictionary file can be read to rebuild a dictionary vector or rebuild the encoding hash table. With a dictionary vector the encoded file can be decoded into the original data file.

Experimental Data
---

Using the dataset provided in the webex team space, we created a testing script `test.sh` which tests the three modes of the program: encoding (e), decoding (d), and interactive (i). Encoding tests read the data file and create a dictionary file and encoded data file. Decoding tests read a dictionary and encoded data file and recreate the data file. A diff is preformed to validate the decoded file. The interactive test reads a dictionary file and queries 1000 lines of the original input. 

System 1's CPU is a AMD Ryzen 7 3800X and System 2's a Intel i7-7700HQ.

| System | Test | Small | Medium | Large |
|-------:|------|-------|--------|-------|
|   | Start size| 932K  | 9.2M   | 180M  |
|   | Total End Size | 486k | 4.9M   | 95.9M |
|   | Dictionary size| 8k |  80k   | 1.56M |
|   | Data size |  478k | 4.8M   | 94.3M |
| 1 |Time e (Real)| 0.012s| 0.154s| 4.496s| 
|   |Time d (Real)| 0.005s| 0.050s| 1.427s|
|   |Time i (Real)| 0.001s| 0.002s| 0.366s|
| 2 |Time e (Real)| 0.022s| 0.300s| 14.756s| 
|   |Time d (Real)| 0.009s| 0.087s| 4.049s | 
|   |Time i (Real)| 0.002s| 0.004s| 1.353s|

Analysis and Conclusion
---
The run time of the various tests indicate that the associative data structure implemented provides good performance. The structure of the dictionary file, used for decoding or rebuilding of the hash table, provides particular good performance when decoding due to the cache friendly structure. The use of SIMD intrinsics to store and compare the short string data somewhat simplifies the code and preliminary tests with an older version of the program indicated that performance of the hash table inserts was slightly improved as well.

The tests include reading and rebuilding the data structures which negatively impacts their performance, but the reduced file sizes of the encoded data and dictionary minimize the impact. 

During programming, the hash table size was found to have a great effect on performance, as expected. Without having any knowledge of the data to be encoded, a tree based data structure would be a better choice. However, we where able to devise a heuristic to find an appropriate size for the hash table during encoding. The heuristic looks at the data to be encoded file size to determine the size of hash table. When rebuilding a hash table for query, the number of dictionary entries is used as a hash table size which provides a good result for a hash function with few collisions. The djb2 hash function is used to hash the string. Several SIMD techniques for computing the hash where considered, such as the CRC functionality in later SSE releases, but the low collision provided by djb2 is essential to performance.

An associative data structure was used to implement a dictionary encoder, and provided good performance when encoding and querying. The encoded file and dictionary are both smaller than the data file used to create them. Decoding performance is very efficient. SIMD intrinsics where used for slight performance improvement. The size of the hash table and collisions of the hash function where found to have great performance impacts. 
