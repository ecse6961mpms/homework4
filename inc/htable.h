#ifndef __HTABLE_H__
#define __HTABLE_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <xmmintrin.h>

typedef int hash_t;
typedef int encode_t;

typedef struct htable_t htable_t;
typedef struct bucket_t bucket_t;
typedef size_t(*hashf_t)(__m128i dat);

struct htable_t{
    size_t size;
    bucket_t** table;
    hashf_t hashf;
};

struct bucket_t{
    __m128i dat;
    encode_t loc;
    int count;
    bucket_t* next;
};

void rm_htable(htable_t* table);

htable_t* create_htable(size_t size, hashf_t hashf);
bucket_t* insert_dat(htable_t* table, __m128i dat);
bucket_t* find(htable_t* table, __m128i dat);


#endif
