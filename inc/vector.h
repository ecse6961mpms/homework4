// YES C++ YES I KNOW but c :) 

#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <stdlib.h>
#include <xmmintrin.h>

#define SIZE_INC 128

typedef struct vector_t vector_t;
typedef __m128i element_t;

struct vector_t{
    size_t size;
    size_t capacity;
    element_t* dat;
};

vector_t* new_vector();

void rm_vector(vector_t* vector);
element_t* push_back(vector_t* vector, __m128i dat);


#endif
