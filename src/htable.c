#include "htable.h"

#include <string.h>
#include <stdlib.h>
#include <nmmintrin.h>

htable_t* create_htable(size_t size, hashf_t hashf){
    htable_t* table = calloc(1, sizeof(htable_t));
    if(!table) return NULL;
    table->size = size;
    table->hashf = hashf;
    table->table = calloc(table->size, sizeof(bucket_t*));
    if(!table->table) {
		free(table);
		return NULL;
	}
    // for(int i = 0; i < table->size; i++) table->table[i] = NULL;
    return table;
}

bucket_t* insert_dat(htable_t* table, __m128i dat){
    size_t hashed = table->hashf(dat) % table->size;
    bucket_t* ptr = table->table[hashed];
    // printf("\"%s\" hash is %lu \n", dat, hashed);
    bucket_t* prev = NULL;
    while(1){
        if(ptr == NULL){
            // printf("inserting \"%s\"\n", dat);
            bucket_t* item = malloc(sizeof(bucket_t));
            if(!item){
                printf("alloc issue\n");
                return NULL;
            }
            item->dat = dat;
            item->loc = -1;
            item->count = 0;
            item->next = NULL;
            if(prev != NULL) prev->next = item;
            else table->table[hashed] = item;
            return item;
        }

        if(_mm_movemask_epi8(~_mm_cmpeq_epi8(dat, ptr->dat)) == 0){
        // if(_mm_cmpistrc(dat, ptr->dat, _SIDD_CMP_EQUAL_ORDERED)){
            // printf("found entry for \"%s\"\n",dat);
            return ptr;
        }
        prev = ptr;
        ptr = ptr->next;
    }
    return NULL;
}
bucket_t* find(htable_t* table, __m128i dat){
        bucket_t* ptr = table->table[table->hashf(dat) % table->size];
    do{
        if(!ptr) return ptr;
        if(_mm_movemask_epi8(~_mm_cmpeq_epi8(dat, ptr->dat)) == 0){
        // if(_mm_cmpistrc(dat, ptr->dat, _SIDD_CMP_EQUAL_ORDERED)){
            return ptr;
        }
    }while((ptr = ptr->next));
    return NULL;
}
void rm_htable(htable_t* table){
	bucket_t* ptr = table->table[0];
	bucket_t* ptrn;
	for(int i = 0; i < table->size; i++){
	// iterate through each bucket
		ptr = table->table[i];
		while(ptr){
			ptrn = ptr->next; 
			free(ptr);
			ptr=ptrn;
		}
	}
	free(table->table);
	free(table);
}
