#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "htable.h"
#include "vector.h"

#define HTABLE_SIZE 1000
#define DEBUG 1

size_t hash_function(__m128i dat){//djb2 replace me
    uint8_t* ptr = (uint8_t*)&dat;
    size_t hash = 5381;
    int c;
    while((c = *ptr++)){
        hash = ((hash<<5)+hash)+c;
    }
    return hash;
}

int count_items(htable_t* table){
    int items = 0;
    for(int i = 0; i < table->size; i++){
        bucket_t* ptr = table->table[i];
        while(ptr){
            items++;
            // printf("\"%s\" occurred %u times\n", ptr->dat, ptr->loc);
            ptr = ptr->next;
        }
    }
    return items;
}

int encode_file(FILE* infile, FILE* outfile, htable_t* table, vector_t* dict){
    char buf[16];
    int count = 0;
    int len = 0;
    encode_t i = 0;
    __m128i dat;
    fwrite("ENCO", 1, 4, outfile);
    while(fgets(buf, 16, infile)){
        len = strlen(buf);
        buf[--len] = '\0';
        dat = _mm_setzero_si128();
        strcpy((char*)&dat, buf);

        bucket_t* item = insert_dat(table, dat);
        if(!item){
            printf("insert returned null\n");
            return 1;
        }
        if(item->loc == -1){
            push_back(dict, dat);
            item->loc = i++;
            item->count = 1;
        } else item->count += 1;

        fwrite(&item->loc, sizeof(encode_t), 1, outfile);
        count++;
    }
    return count;
}

void write_dict(FILE* outfile, htable_t* table, vector_t* dict){
    size_t len;
    size_t written;
    __m128i dat;
    bucket_t* item;
    fwrite("DICT", 1, 4, outfile);
    fwrite(&(dict->size), sizeof(dict->size), 1, outfile);
    for(int i = 0; i < dict->size; i++){
        dat = dict->dat[i];
        item = find(table, dat);
        assert(i == item->loc);
        len = strlen((char*)&dat);
        fwrite(&len, 1, 1, outfile);
        fwrite(&item->count, sizeof(int), 1, outfile);
        fwrite((char*)&dat, 1, len, outfile);
        written += len+1;
    }
}

int read_dict(FILE* infile, vector_t* dict){
    char buf[16];
    fread(buf, 1, 4, infile);
    buf[4] = '\0';
    printf("read: %s \n", buf);
    if(strcmp("DICT", buf) != 0){
        printf("Not a dictionary file!\n");
        return -1;
    }
    size_t size;
    fread(&size, sizeof(size), 1, infile);
    printf("dictionary has %lu elements\n", size);
    uint8_t len;
    __m128i dat;
    int count;
    while(fread(&len, 1, 1, infile)){
        fread(&count, sizeof(int), 1, infile);
        fread(buf, 1, len, infile);
        buf[len]='\0';
        dat = _mm_setzero_si128();
        strcpy((char*)&dat, buf);
        // printf("length %u, string %s \n", len, buf);
        push_back(dict, dat);
    }

    return 0;
}

int decode_file(FILE* infile, FILE* outfile, vector_t* dict){
    char buf[16];
    fread(buf, 1, 4, infile);
    buf[4] = '\0';
    printf("read: %s \n", buf);
    if(strcmp("ENCO", buf) != 0){
        printf("Not an encoded file!\n");
        return -1; 
    }
    encode_t loc; 
    int len;
    __m128i dat;
    while(fread(&loc, sizeof(encode_t), 1, infile)){
        // printf("read loc %u : %s \n", loc, dict->dat[loc].dat);
        dat = dict->dat[loc];
        len = strlen((char*)&dat);
        fwrite((char*)&dat, 1, len, outfile);
        fwrite("\n", 1, 1, outfile);
    }

    return 0;
}

htable_t* read_dict_hash(FILE* infile, vector_t* dict){
    char buf[16];
    fread(buf, 1, 4, infile);
    buf[4] = '\0';
    printf("read: %s \n", buf);
    if(strcmp("DICT", buf) != 0){
        printf("Not a dictionary file!\n");
        return NULL;
    }
    size_t size;
    fread(&size, sizeof(size), 1, infile);
    printf("dictionary has %lu elements\n", size);
    htable_t* htable = create_htable(size, hash_function);
    assert(htable);
    uint8_t len;
    __m128i dat;
    bucket_t* item;
    int i = 0;
    int count;
    while(fread(&len, 1, 1, infile)){
        fread(&count, sizeof(int), 1, infile);
        fread(buf, 1, len, infile);
        buf[len]='\0';
        dat = _mm_setzero_si128();
        strcpy((char*)&dat, buf);
        // printf("length %u, string %s \n", len, buf);
        push_back(dict, dat);
        item = insert_dat(htable, dat);
        item->loc = i++;
        item->count = count;
    }

    return htable;
}

int encode_mode(int argc, char** argv){
    if(argc < 5){
        printf("Usage: %s e infile dict_out encode_out\n", argv[0]);
        return 1;
    }

    FILE* infile = fopen(argv[2], "r");
    if(!infile){
        printf("could not open \"%s\"\n", argv[2]);
        return 1;
    }

    FILE* outfile = fopen(argv[4], "wb");
    if(!infile){
        printf("could not open \"%s\"\n", argv[4]);
        return 1;
    }


	// Get hashtable size for the file size
	// We're approximating as linecount / 1024
	int tablesize = 0;
	for(char c=getc(infile); c!=EOF; c=getc(infile)){
		if(c=='\n') tablesize += 1;
	}
	//divide by 1024
	tablesize = tablesize >> 10;
	// Use defined value as minimum
	if(tablesize <= HTABLE_SIZE) {
		if(DEBUG) printf("%d table size too small, using default\n", tablesize);
		tablesize = HTABLE_SIZE;
	}
	//get to start of file
	rewind(infile);


    htable_t* table = create_htable(tablesize, hash_function);
    if(!table){
        printf("table alloc failed.\n");
        return 1;
    }
    vector_t* dict = new_vector();

    encode_file(infile, outfile, table, dict);

    fclose(infile);
    fclose(outfile);

    FILE* outfile_dict = fopen(argv[3], "wb");
    if(!outfile_dict){
        printf("could not open \"%s\"\n", argv[3]);
        return 1;
    }

    write_dict(outfile_dict, table, dict);

    fclose(outfile_dict);

    int items = count_items(table);
    printf("%d unique items\n",items);

    printf("sizeof element_t: %lu \n",sizeof(element_t));
    printf("items in vector: %lu\n", dict->size);
    rm_vector(dict);
	rm_htable(table); 
    return 0;
}

int decode_mode(int argc, char** argv){
    if(argc < 5){
        printf("Usage: %s e dict encode outfile\n", argv[0]);
        return 1;
    }
    FILE* infile_dict = fopen(argv[2], "rb");
    if(!infile_dict){
        printf("could not open \"%s\"\n", argv[2]);
        return 1;
    }

    vector_t* dict_r = new_vector();
    read_dict(infile_dict, dict_r);
    fclose(infile_dict);

    FILE* infile = fopen(argv[3], "rb");
    if(!infile){
        printf("could not open \"%s\"\n", argv[3]);
        return 1;
    }
    FILE* outfile = fopen(argv[4], "w");
    if(!outfile){
        printf("could not open \"%s\"\n", argv[4]);
        return 1;
    }

    decode_file(infile, outfile, dict_r);

    fclose(infile);
    fclose(outfile);
	rm_vector(dict_r);
    return 0;
}
int interactive_mode(int argc, char** argv){
	if(argc < 3 ){
        printf("Usage: %s i dict\n", argv[0]);
        return 1;
    }
    
	FILE* infile_dict = fopen(argv[2], "rb");
    if(!infile_dict){
        printf("could not open \"%s\"\n", argv[2]);
        return 1;
    }

    vector_t* dict_r = new_vector();
    htable_t* htable = read_dict_hash(infile_dict, dict_r);
    fclose(infile_dict);

	// check to see if the string exists

    char buf[16]; 
    int len;
    __m128i dat;
    bucket_t* item;
    printf("enter query (Ctrl-D to stop): ");
    while(fgets(buf, 16, stdin)){
        len = strlen(buf);
        buf[--len] = '\0';
        dat = _mm_setzero_si128();
        strcpy((char*)&dat, buf);

        item = find(htable, dat);
        if(item){
            printf("%s is in file %u times with an encoded value of %u\n", buf, item->count, item->loc);
        } else{
            printf("%s is not in the file\n", buf);
        }
        printf("enter query (Ctrl-D to stop): ");
    }
    printf("\n");

	rm_vector(dict_r);
    rm_htable(htable);

	return 0;
}

int main(int argc, char** argv){
    if(argc < 2){
        printf("usage: %s [e, d, i] ...\n", argv[0]);
    }

    if(argv[1][0] == 'e') return encode_mode(argc, argv);
    if(argv[1][0] == 'd') return decode_mode(argc, argv);
	if(argv[1][0] == 'i') return interactive_mode(argc, argv);

    printf("unkown mode \"%s\"\n", argv[1]);
    return 1;
}
