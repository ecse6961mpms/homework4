#include "vector.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

vector_t* new_vector(){
    vector_t* vector = malloc(sizeof(vector_t));
    vector->size = 0;
    vector->capacity = SIZE_INC;
    vector->dat = malloc(sizeof(element_t)*vector->capacity);
    if(!vector->dat){
        free(vector);
        return NULL;
    }
    return vector;
}

element_t* push_back(vector_t* vector, __m128i dat){
    if(vector->size+1 < vector->capacity){
        vector->dat[vector->size] = dat;
        vector->size++;
        return &(vector->dat[vector->size-1]);
    } else {
        element_t* new_dat = malloc(sizeof(element_t)*(vector->capacity + SIZE_INC));
        if(!new_dat){
            printf("alloc error! im gonna segfault soon!\n");
        }

        memcpy(new_dat, vector->dat, sizeof(element_t)*vector->capacity);
        free(vector->dat);
        vector->dat = new_dat;
        vector->capacity += SIZE_INC;
        return push_back(vector, dat);
    }
    return NULL;
}

void rm_vector(vector_t* vector){
	free(vector->dat);
	free(vector);
}
