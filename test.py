import sys

infile = open(sys.argv[1])
counts = {}
entries = 0
maxlen = 0
for line in infile:
    entries += 1
    dat = line.strip()
    if len(dat) > maxlen:
        maxlen=len(dat)
    if dat in counts:
        counts[dat] += 1
    else:
        counts[dat] = 1

print(f"entries: {entries}")
print(f"unique items: {len(counts)}")
print(f"Longest entry: {maxlen}")
# print(counts)
