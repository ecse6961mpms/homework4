#! /bin/bash


test(){
echo "Encoding $1 now --------------------------"
time ./dict e $1 temp.dict temp.dat

echo "Decoding $1 now --------------------------"
time ./dict d temp.dict temp.dat tempout.txt

echo "Running diff:"
if ! diff -q $1 tempout.txt &>/dev/null; then
	>&2 echo "ERROR: Files Differ"
else
	echo "Files match"
	# find the size difference 
	sizein=$(wc -c <"$1")
	sizedict=$(wc -c <"temp.dict")
	sizedat=$(wc -c <"temp.dat")
	sizetot=$((sizedict + sizedat))
	delta=$((sizein - sizetot))
	shrink=$((100 - (sizetot * 100 / sizein)))	

	echo "Initial Size: $sizein"
	echo "Final Size:   $sizetot"
	echo "Dict Size:    $sizedict"
	echo "Data Size:    $sizedat"
	echo "Reduction:    $delta "
	echo "Compression:  ~$shrink%" 
fi

#build test file for query
head -n 1000 $1 > tempquery.txt
echo "Querying 1000 lines from $1 now ----------"
time ./dict i temp.dict < tempquery.txt > /dev/null

# delete the temp files if the second arg is present
if [ -n "$2" ]; then
	rm temp.dict temp.dat tempout.txt tempquery.txt
fi
}

if [ -n "$1" ]; then
	test $1 $2
else
	echo "Running standard tests provided"
	test Small-Size-Column.txt r
	test Medium-Size-Column.txt r
	test Large-Size-Column.txt r
fi
